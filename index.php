<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shifteast
 */

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
		$hero_title = "Experiences that move you";
		$hero_subtitle = "";
		$hero_image = get_template_directory_uri()."/ressources/images/village.jpg";
		$small_hero = false;
		include(locate_template('page-header.php'));
		?>

    <div class="about">
      <div class="wrap">
        <h2>What we do</h2>
        <span class="subtitle">Japan’s best assets are also its least appreciated.</span>
        <p class="two-columns-text">
          VISION – We’re creating a trustworthy global platform that uses rich media to promote offline destinations.
          Our ecosystem includes content, services, and local location production to enrich travel experiences,
          preserve historical properties, and provide sustainable development for cities, towns and local communities.
          Most important to our way of working is emphasizing Japan’s most valuable asset of communities.
          By understanding the needs and desires of local communities, we design our projects around them, and then
          create a vision that will attract the right kind of visitors and enable experiences that benefit a larger
          group of people.
        </p>
      </div>
    </div>
    <div class="projects" id="projects">
      <div class="wrap">
        <h2>Our Services</h2>
        <span class="subtitle">Things we are working on.</span>
        <div class="projects-list">
          <div class="project">
            <div class="project-image" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/photographer-snow.jpg"></div>
            <div class="project-details">
              <h2>Content Production</h2>
              <span class="subtitle">
                We craft high-quality stories
              </span>
              <p>
                Since 2015, and through 2018, we have commissions to produce countrywide travel and culture content for
                a global audience. Sending writers and photographers to every corner of Japan,
                we are not only able to create compelling features and images, but are creating strong networks and
                discovering new areas for our other projects.
              </p>
            </div>
          </div>
          <div class="project right">
            <div class="project-image" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/99+1.jpg"></div>
            <div class="project-details">
              <h2>Consulting</h2>
              <span class="subtitle">
                A unique publication for traveling through art, design and architecture.
              </span>
              <p>
                Commissioned by JNTO, 99+1 is a pocket guidebook created to satisfy modern travelers interested in art,
                design and architecture in Japan.
                The publication collects 99 unique destinations across Japan and is designed to inspire higher-end
                travel agencies to recommend more exclusive itineraries for their clients.
              </p>
            </div>
          </div>
          <div class="project">
            <div class="project-image" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/AIR.jpg"></div>
            <div class="project-details">
              <a href="/air" class="project-title">Town Development</a>
              <span class="subtitle">Inviting artists to experience Japanese traditional craftmanship.</span>
              <p>
                We’re creating a trustworthy global platform that uses rich media to promote offline destinations. Our
                ecosystem includes content, services, and local location production to enrich travel experiences,
                preserve historical properties,
                and provide sustainable development for cities, towns and local communities.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="team">
      <div class="wrap">
        <h2>Who we are</h2>
        <span class="subtitle">
          A team of passion-driven people.
        </span>
        <div class="team-members">
          <div class="team-member">
            <div class="portrait" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/team/michael.jpg">
            </div>
            <div class="user-infos">
              <h2>Michael</h2>
              <div class="role">
                Director
              </div>
            </div>
          </div>
          <div class="team-member">
            <div class="portrait" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/team/jordy.jpg">
            </div>
            <div class="user-infos">
              <h2>Jordy</h2>
              <div class="role">
                CTO
              </div>
            </div>
          </div>
          <div class="team-member">
            <div class="portrait" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/team/yoko.jpg">
            </div>
            <div class="user-infos">
              <h2>Yoko</h2>
              <div class="role">
                Creative Director
              </div>
            </div>
          </div>
          <div class="team-member">
            <div class="portrait" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/team/don.jpg">
            </div>
            <div class="user-infos">
              <h2>Don</h2>
              <div class="role">
                Media Manager
              </div>
            </div>
          </div>
          <div class="team-member">
            <div class="portrait" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/team/diego.jpg">
            </div>
            <div class="user-infos">
              <h2>Diego</h2>
              <div class="role">
                Photographer
              </div>
            </div>
          </div>
          <div class="team-member">
            <div class="portrait" data-bg="<?php echo get_template_directory_uri(); ?>/ressources/images/team/thomas.jpg">
            </div>
            <div class="user-infos">
              <h2>Thomas</h2>
              <div class="role">
                Front-end Developer
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php 
		$white = '';
		include(locate_template('newsletter-section.php'));
		?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();