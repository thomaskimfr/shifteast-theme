<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package shifteast
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php

		?>

		<?php
		while ( have_posts() ) :
			the_post();
			$hero_title = get_the_title();
			$categories = get_the_category(); 
			$temp = array();
			foreach($categories as $category) {
				$temp_category_markup = "<a href='".get_category_link( $category->term_id )."'>".$category->name."</a>";
				array_push($temp,$temp_category_markup); 
			}
			$categories_markup = "";
			foreach($temp as $category_markup) {
				$categories_markup .= $category_markup;
			}
			if(!is_page()) {
				$hero_subtitle = "By ".get_the_author()."&nbsp; | &nbsp;In ".$categories_markup."&nbsp; | &nbsp;20min";
			} else {
				$hero_subtitle = "";
			}
			$hero_image = get_the_post_thumbnail_url($post->ID, 'full');
			$small_hero = false;
			include(locate_template('page-header.php'));
			?>
			<div class="single-post-container">
				<div class="container">
					<?php
					the_content();
					?>
				</div>
			</div>
			<?php
			if(!is_page()) {
			?>
			<div class="post-navigation-container">
				<?php 
				$prev_post = get_previous_post();
				if(!empty( $prev_post )) {
				?>
				<a href="<?php echo get_permalink($prev_post->ID); ?>" class="previous-post-container">
					<div class="background" data-bg="<?php echo get_the_post_thumbnail_url($prev_post->ID, 'full'); ?>"></div>
					<div class="top-label">
						Previous Post
					</div>
					<h3><?php echo $prev_post->post_title; ?></h3>
				</a>
				<?php
				}
				$next_post = get_next_post();
				if(!empty( $next_post )) {
				?>
				<a href="<?php echo get_permalink($next_post->ID); ?>" class="next-post-container">
					<div class="background" data-bg="<?php echo get_the_post_thumbnail_url($next_post->ID, 'full'); ?>"></div>
					<div class="top-label">
						Next Post
					</div>
					<h3><?php echo $next_post->post_title; ?></h3>
				</a>
				<?php
				}
				?>
			</div>
			<?php
			}

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				// comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
