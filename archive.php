<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shifteast
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		$hero_title = "Blog";
		$hero_subtitle = "All the articles written in ".get_queried_object()->name;
		$hero_image = "";
		$small_hero = true;
		include(locate_template('page-header.php'));
		?>

		<?php if ( have_posts() ) : ?>

			<div class="container">
				<div class="row">
					<?php
					while ( have_posts() ) :
						the_post();
						get_template_part( 'components/listed-post' );
					endwhile; 
					?>
				</div><!-- row -->
			</div><!-- ontainer -->

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer();
