<div class="mailing-list <?php echo $white; ?>">
    <div class="wrap">
        <h2>Follow Us</h2>
        <span class="subtitle">If you like what we are doing,<br/> make sure to subscribe to our newsletter !</span>
        <br/>
        <a href="http://eepurl.com/c6S02b" class="ghost-btn">
            Subscribe to the newsletter
        </a>
    </div>
</div>