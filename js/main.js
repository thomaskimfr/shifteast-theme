jQuery(document).ready(function ($) {
  $('*[data-bg]').each(function () {
    var bg_src = $(this).attr('data-bg');
    $(this).css('background-size', 'cover');
    $(this).css('background-position', 'center');
    $(this).css('background-image', 'url(' + bg_src + ')');
  });

  $('.page-hero').imagesLoaded({
    background: true
  }, function () {
    console.log('loaded');
    $('.page-hero').find('.background').addClass('loaded');
  });

  $(window).on('scroll', function () {
    var scrollTop = $(this).scrollTop();

    $('.page-hero .background').css('transform', 'translate3d(0,' + scrollTop / 2 + 'px,0)');
    $('.page-hero .scroll-downs').css('opacity', 1 - scrollTop / 200);
    $('.page-hero .content .subtitle').css('opacity', 1 - scrollTop / 100);
    $('.page-hero .content h1').css('transform', 'translate3d(0,' + scrollTop / 3 + 'px,0)');
    $('.page-hero .content h1').css('opacity', 1 - (scrollTop / $('.page-hero').outerHeight()));

    /*
    $('.project').each(function() {
        if($(window).outerWidth() > 660) {
            var $project = $(this);
            var $project_details = $(this).find('.project-details');
            var project_top = $project.offset().top;
            var project_bottom = project_top + $project.outerHeight();
            var scrollTopBottom = scrollTop + $(window).outerHeight();
            var relativeScrollTop = scrollTop - project_top;
            var visible = false;
            if(project_top > scrollTop && project_top < scrollTopBottom) {
                visible = true;
            }
            if(project_top < scrollTop && project_bottom > scrollTop) {
                visible  = true;
            }
            if(!visible) {
            } else {
                $project_details.css('transform', 'translate3d(0,'+relativeScrollTop/14+'px,0)')
            }
        }
    });
    */

    $('.listed-post-container').each(function () {
      if ($(window).outerWidth() > 660) {
        var $post = $(this);
        var $post_details = $(this).find('.post-details');
        var post_top = $post.offset().top;
        var post_bottom = post_top + $post.outerHeight();
        var scrollTopBottom = scrollTop + $(window).outerHeight();
        var relativeScrollTop = scrollTop - post_top;
        var visible = false;
        if (post_top > scrollTop && post_top < scrollTopBottom) {
          visible = true;
        }
        if (post_top < scrollTop && post_bottom > scrollTop) {
          visible = true;
        }
        if (!visible) {} else {
          $post_details.css('transform', 'translate3d(0,' + relativeScrollTop / 14 + 'px,0)')
        }
      }
    });
  });

  if ($(window).outerWidth() > 660) {
    $('.listed-post-container').each(function () {
      var scrollTop = $(window).scrollTop();
      var $post = $(this);
      var $post_details = $(this).find('.post-details');
      var post_top = $post.offset().top;
      var post_bottom = post_top + $post.outerHeight();
      var scrollTopBottom = scrollTop + $(window).outerHeight();
      var relativeScrollTop = scrollTop - post_top;
      var visible = false;
      if (post_top > scrollTop && post_top < scrollTopBottom) {
        visible = true;
      }
      if (post_top < scrollTop && post_bottom > scrollTop) {
        visible = true;
      }
      if (!visible) {} else {
        $post_details.css('transform', 'translate3d(0,' + relativeScrollTop / 14 + 'px,0)')
      }
    });
  }

  $('.listed-post-container').each(function (index) {
    if (index % 2 !== 0) {
      $(this).addClass('right');
    }
  });
});