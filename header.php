<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package shifteast
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'shifteast' ); ?></a>

    <div class="page-top-bar" :class="stickyNav ? 'sticky' : ''">
        <div class="branding">
            <img src="<?php echo get_template_directory_uri(); ?>/ressources/images/se-logo-simple.png" class="se-logo">
        </div>
        <div class="language-switcher">
            <div class="current-language">
                <div class="ghost-btn">
                    English
                </div>
            </div>               
        </div>
        <div class="page-nav">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			?>
			<div class="small-menu-trigger">
				<i class="ionicons ion-navicon"></i>
			</div>
		</div>
	</div>

	<div class="small-menu-container">
		<div class="small-menu-trigger">
			<i class="ionicons ion-android-close"></i>
		</div>
		<div class="small-menu">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			?>
		</div>
	</div>

	<div id="content" class="site-content">
